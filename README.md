#PYTHON-coding for security device

import serial
import time
import RPi.GPIO as GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.IN) #Read output from PIR motion sensor

SERIAL_PORT="/dev/ttyS0"
ser = serial.Serial(SERIAL_PORT, baudrate=115200, timeout=5)

area_locked = False


list_of_lists=[]
with open ("contacts.txt","r") as memory_data:
        for line in memory_data:
                inner_list = [elt.strip() for elt in line.split(',')]
                list_of_lists.append(inner_list)
        contact_1= str(list_of_lists[0]).strip("['']")
        print contact_1
        contact_2= str(list_of_lists[1]).strip("['']")
        print contact_2
        contact_3= str(list_of_lists[2]).strip("['']")
        print contact_3
        contact_4= str(list_of_lists[3]).strip("['']")
 #       print contact_4
        contact_5= str(list_of_lists[4]).strip("['']")
#        print contact_5

def send_sms(contact,message):
        ser.write("AT+CMGF=1\r")
        time.sleep(1)
        ser.write('AT+CMGS="' + contact.encode() + '"\r')
        time.sleep(1)
        ser.write(message.encode() + "\r")
        time.sleep(1)
        reply=ser.read(ser.inWaiting())
        print (reply)
        ser.write("\x1A")
 time.sleep(1)
        reply =ser.read(ser.inWaiting())
#       print reply
        print "Message send successfully!!!"



def recieve_sms(SERIAL_PORT):
        SERIAL_PORT = "/dev/ttyS0"
        ser= serial.Serial(SERIAL_PORT, baudrate =115200, timeout=5)

        ser.write("AT\r")
        time.sleep(1)
        ser.write("AT+CMGF=1\r")
        time.sleep(1)
        ser.write('AT+CMGDA="DEL ALL "\r')
        time.sleep(1)
        print "Listening to the incoming msg..."

        while True:
  reply=ser.read(ser.inWaiting())
                if reply != "":
                        ser.write("AT+CMGR=1\r")
                        time.sleep(1)
                        reply=str.strip(ser.read(ser.inWaiting()))
                        msg_content=reply[42:54]
                        print "msg_content:",msg_content

def reject_call():
       #        time.sleep(5)
                ser.write("ATH\r")
                time.sleep(1)
                print "call is cut"


def give_call_ringing(contact):
        while True:
                ser.write("ATD" + contact.encode() +";\r")
                time.sleep(1)
                reply=str.strip(ser.read(ser.inWaiting()))
                print reply
                if str(reply =="OK"):
                        print 'Phone ringing'
                        time.sleep(20)
                        if str(reply =="NOANSWER") or str(reply == "BUSY"):
                                print 'User is busy'
                                break
                #       break
        #       return  reply



def check_pir(i):
        count = 0
        while  True:
                i=GPIO.input(11)
                if i==0:                 #When output from motion sensor is LOW
                        print "No motion_detected",i
                        time.sleep(1)
#                       count +=1
#                       time.sleep(0.1)
#                       if count == 10:
                elif i==1:               #When output from motion sensor is HIGH
                        print "Motion detected",i
                        time.sleep(1)
                        send_sms(contact_1, "Motion Found")
                        time.sleep(5)
                        send_sms(contact_3, "Motion Found ")
                        time.sleep(5)
                        give_call_ringing (contact_2)
                        #break


while True:
        send_sms(contact_2, "Smartify Technologies                           Security Device Activated                     Successfully")
        ser.write("AT+CLIP=1\r\n")
        time.sleep(0.1)
        while True:
                received_no=str.strip(ser.read(ser.inWaiting()))
                time.sleep(1)
                if str(received_no == "RING"):
                        R_C = received_no[19:29]
                        print(R_C)
                        if (R_C == contact_1) or (R_C == contact_2) or (R_C == contact_3) or (R_C == contact_4) or (R_C == contact_5):
                                print "contact matched"
                                reject_call()
                                if area_locked == False:
                                        area_locked =True
                                        print "area is unlocked"
                                        send_sms(contact_2, "Smartify Technologies                   AREA UN-LOCKED-Succesfully")
#                                       check_pir(11)
                                elif area_locked ==True:
                                        area_locked=False
                                        print "area is locked"
                                        send_sms(contact_2, "Smartify Technologies                   AREA LOCKED-Succesfully")
                                else :
                                        area_locked = False
                                        time.sleep(2)
#                       elif (temp_word != contact_1) or (temp_word != contact_2):
#                               print "No contact matched"

